    // spot の各キーの値を変数へ格納
    var registered_lat = parseFloat(spot.latitude);    // 緯度
    var registered_lon = parseFloat(spot.longitude);   // 経度

    // マップ初期設定　@param center_lat, center_lon, initial_zoom, maxZoom, minZoom
    var mymap = initialize_map(registered_lat, registered_lon, 15, 19, 12);　// 地図の中心：スポットの緯度経度

    // マーカーに緯度経度を指定し、マップに追加する
    popup_content = popup_content_registered;
    set_single_marker(registered_lat, registered_lon, popup_content);

    // 手動（クリック）マーカーセット(マップ上のクリックした位置にマーカーをセットする)
    mymap.on('click', function(e){
        popup_content = popup_content_edit;
        set_manual_marker(e, my_icon, popup_content);
    });