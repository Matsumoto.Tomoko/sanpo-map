    var markers = {};　// 複数マーカーの featureGroup 用オブジェクト
    
    /****************************************************************
    * マップ初期設定                                                * 
    * @param center_lat, center_lon, initial_zoom, maxZoom, minZoom *
    *****************************************************************/
    var mymap = initialize_map(lat_of_tokyo_metropolitan_government, lon_of_tokyo_metropolitan_government, 12, 19, 4); // 地図の中心：東京都庁

    /*******************
    * クリックイベント * 
    ********************/
    // 検索ボタンクリックイベント 
    document.getElementById("serach_btn").onclick = function() {
        fetch_spot_data();
    };
    
    // リセットボタンクリックイベント 
    document.getElementById("reset_btn").onclick = function() {
        reset_input_name();
        reset_set_value();
        reset_map();
    };

    // マップクリックイベント(マップ上のクリックした位置にマーカーをセットする)
    mymap.on('click', function(e){
        popup_content = popup_content_create;
        set_manual_marker(e, candidate_icon, popup_content);
    });

    /********
    * 関数  * 
    *********/
    /*
     * スポット検索(OpenStreetMapAPIからスポット情報を取得する)
     */
    function fetch_spot_data(){
        // マップとフォームにセットされた値をリセットする（クリアボタンを使用せず、スポット名を直接訂正して再検索した場合の対応）
        reset_set_value();
        reset_map();

        // 検索フォームに入力のスポット名を取得→文字列変換
        var input_spot_name = document.getElementById("name").value;
        var spot_name_converted_alphanumeric = full2half_alphanumeric(input_spot_name);
        var spot_name = half2full_kana(spot_name_converted_alphanumeric);
        // 変換後の文字列をフォームに入力し直す
        document.getElementById("name").value = spot_name;
        // スポット名を代入し URL に変換
        var url = encodeURI('https://nominatim.openstreetmap.org/search/?q=' + spot_name + '&countrycodes=JP&format=json');

        // Fetch APIで URL のリクエストを投げレスポンスをJSONで取得し、レスポンスの件数により関数を呼び出す or メッセージを出力する
        fetch(url)
        .then(function (response){
            if (response.ok) {
                return response.json();
            }
            // throw new Error('検索できませんでした。');
        })
        .then(function (json_response) {
            var spot_list = json_response;

            var search_result = document.getElementById('search_result');
            search_result.insertAdjacentHTML('beforeend', '<li>検索結果は、' + spot_list.length + '件です</li>');

            if (spot_list.length === 0) {
                search_result.insertAdjacentHTML('beforeend', '<li>' + search_result_message_0_cases + '</li>');
            }else if(spot_list.length === 1){
                search_result.insertAdjacentHTML('beforeend', '<li>' + search_result_message_manual_marker_info + '</li>');
                
                mymap.setView([spot_list[0].lat, spot_list[0].lon], 15);
                popup_content = popup_content_create;
                set_single_marker(spot_list[0].lat, spot_list[0].lon, popup_content);

            }else{
                search_result.insertAdjacentHTML('beforeend', '<li>' + search_result_message_multiple_cases + '</li>');
                search_result.insertAdjacentHTML('beforeend', '<li>' + search_result_message_manual_marker_info + '</li>');
                set_markers(spot_list);
            }
        })
        .catch(function (e) {
            console.log(e.message);
        });
    }
    
    /*
     * 複数マーカーセット(スポット情報リストからそれぞれ緯度経度を取得し、複数マーカーをマップにセットする)
     */
    function set_markers(spot_list){
        markers = L.featureGroup();
        for(var i = 0; i < spot_list.length; i++)
        {
            L.marker([spot_list[i].lat, spot_list[i].lon], {icon: candidate_icon})
                .addTo(markers);
        }
        markers
            .addTo(mymap)
            .bindPopup('ここを登録する')
            // マーカークリックイベント
            .on( 'click', function(e) {
                if(Object.keys(manual_marker).length !== 0){
                    mymap.removeLayer(manual_marker);
                }
                if (Object.keys(selected_marker).length !== 0) {
                    selected_marker.setIcon(candidate_icon);
                }
                var clicked_marker_lat = e.latlng.lat;
                var clicked_marker_lon = e.latlng.lng;
                // fitBounds した Zoom レベルが初期値より少ない場合は 15 に設定し、取得した緯度・経度にマップの中心を移動する
                if (mymap.getZoom() <= 12) {
                    mymap.setView([clicked_marker_lat,clicked_marker_lon], 15);
                }
                selected_marker = e.layer;
                selected_marker.setIcon(selected_icon);
                document.getElementById('latitude').value = clicked_marker_lat;
                document.getElementById('longitude').value = clicked_marker_lon;
            });
        mymap.fitBounds(markers.getBounds(), {maxZoom: 17});
    }
    /*
     * 入力されたスポット名の値をクリアする
     */
     function reset_input_name(){
        document.getElementById("name").value = "";
    }
    /*
     * フォームにセットされた値をクリアする
     */
     function reset_set_value(){
        document.getElementById("search_result").textContent = "";
        document.getElementById('latitude').value = "";
        document.getElementById('longitude').value = "";
        document.getElementById("comment").value = "";
    }
    /*
     * マップを初期設定にリセットする
     */
    function reset_map(){
        if(Object.keys(markers).length !== 0){
            mymap.removeLayer(markers);
        }
        if(Object.keys(selected_marker).length !== 0){
            mymap.removeLayer(selected_marker);
        }
        if(Object.keys(manual_marker).length !== 0){
            mymap.removeLayer(manual_marker);
        }

        mymap.setView([lat_of_tokyo_metropolitan_government, lon_of_tokyo_metropolitan_government],12); // 地図を初期設定にセットし直す
    }