    // spot の各キーの値を変数へ格納
    var registered_lat = parseFloat(spot.latitude);    // 緯度
    var registered_lon = parseFloat(spot.longitude);   // 経度
    var spot_name = spot.name;        // スポット名

    // マップ初期設定　@param center_lat, center_lon, initial_zoom, maxZoom, minZoom
    var mymap = initialize_map(registered_lat, registered_lon, 15, 19, 12);　// 地図の中心：スポットの緯度経度

    // マーカーに緯度経度・スポット名を指定し、マップに追加する
    L.marker([registered_lat, registered_lon], {icon: my_icon})
        .addTo(mymap)
        .bindPopup("<p>" + spot_name + "</p>")
        .openPopup();