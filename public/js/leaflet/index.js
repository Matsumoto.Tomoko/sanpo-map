    var registered_lat_list = [];   // 緯度用配列
    var registered_lon_list = [];   // 経度用配列
    var spot_name_list = [];        // スポット名用配列

    // spots が0件の場合
    if (spots.length === 0) {
        // 表示切り替え
        document.getElementById("no_spot").style.display="block";
        document.getElementById("has_spot").style.display="none";
    // spots が1件以上の場合
    }else{
        // 表示切り替え
        document.getElementById("no_spot").style.display="none";
        document.getElementById("has_spot").style.display="block";
        
        // マップ初期設定
        var mymap = L.map('mapid');
        mymap = set_max_bounds_to_map(mymap);           // マップの表示範囲の上限設定
        mymap = set_tilelayer_to_map(mymap, 19, 8);     // マップのtilelayer設定

        // オブジェクトのプロパティの値をそれぞれの配列に格納する
        spots.forEach(function( spot ) {
            registered_lat_list.push(spot.latitude);
            registered_lon_list.push(spot.longitude);
            spot_name_list.push(spot.name);
        });
        
        // スポット情報リストからそれぞれ緯度経度・スポット名を取得し、複数マーカーをマップにセットする
        var markers = L.featureGroup();
        for (var i = 0; i < spots.length; i++){
            L.marker([ registered_lat_list[i], registered_lon_list[i] ], {icon: my_icon})
                .bindPopup("<p>" + spot_name_list[i] + "</p>")
                .addTo(markers);
        }
        markers
            .addTo(mymap);
        // マップの範囲をマーカーにフィットさせる
        mymap.fitBounds(markers.getBounds());
    }