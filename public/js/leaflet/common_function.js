    /*******
    * 定数 * 
    ********/
    // 緯度経度
    const northernmost_lat_of_Japan = 45.3326;  // 日本最北端の緯度
    const northernmost_lon_of_Japan = 148.4508; // 日本最北端の経度
    const westernmost_lat_of_Japan  = 24.2705;  // 日本最西端の緯度
    const westernmost_lon_of_Japan  = 122.5557; // 日本最西端の経度
    const lat_of_tokyo_metropolitan_government = 35.68944;  // 東京都庁の緯度
    const lon_of_tokyo_metropolitan_government = 139.69167; // 東京都庁の経度
    // ポップアップコンテンツ
    const popup_content_create = "ここを登録する";
    const popup_content_edit = "ここに変更する";
    const popup_content_registered = "現在の登録位置";
    // 検索結果によるメッセージ
    const search_result_message_0_cases            = "検索語句を変えて検索し直すか、マップをクリックしてマーカーを設置してください";
    const search_result_message_multiple_cases     = "表示されたマーカーの中から該当のマーカーをクリックして、スポットを指定してください";
    const search_result_message_manual_marker_info = "表示されたマーカー以外の地点を登録したい場合は、マップをクリックして新たにマーカーを設置してください";

    /**************
    * 共通の変数  * 
    ***************/
    var selected_marker = {};   // 選択中のマーカー用オブジェクト
    var manual_marker   = {};   // クリックで設置したマーカー用オブジェクト
    var popup_content   = "";   // マーカーからポップアップする内容
    var my_icon         = L.divIcon({className: 'my_icon'});         // 登録中のマーカー用アイコン
    var candidate_icon  = L.divIcon({className: 'candidate_icon'});  // 登録候補のマーカー用アイコン
    var selected_icon   = L.divIcon({className: 'selected_icon'});   // 現在選択中のマーカー用アイコン

    /*************
    * 共通の関数 * 
    **************/
    /*
     * マップ初期設定
     * @param center_lat, center_lon, initial_zoom, maxZoom, minZoom
     * @return mymap
     */
     function initialize_map(center_lat, center_lon, initial_zoom, maxZoom, minZoom) {
        var mymap = L.map('mapid', {
            center: [center_lat,center_lon],
            zoom: initial_zoom,
        });
        // マップの表示範囲の上限設定
        mymap = set_max_bounds_to_map(mymap);
        // マップのtilelayer設定
        mymap = set_tilelayer_to_map(mymap, maxZoom, minZoom);

        return mymap;
    }

    /*
     * マップの表示範囲の上限設定
     * @param mymap
     * @return mymap
     */
    function set_max_bounds_to_map(mymap) {
        mymap.setMaxBounds([
            [northernmost_lat_of_Japan, northernmost_lon_of_Japan], // 日本最北端
            [westernmost_lat_of_Japan, westernmost_lon_of_Japan]    // 日本最西端
        ]);

        return mymap;
    }

    /*
     * マップのtilelayer設定
     * @param mymap, maxZoom, minZoom
     * @return mymap
     */
    function set_tilelayer_to_map(mymap, maxZoom, minZoom) {
        var tileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
            attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            maxZoom: maxZoom,
            minZoom: minZoom,
            });
        tileLayer.addTo(mymap);

        return mymap;
    }

    /*
     * シングルマーカーセット(マップ上の、渡された緯度経度地点に、マーカーを一つセットする)
     */
    function set_single_marker(param_lat, param_lon, popup_content){
        selected_marker = L.marker([param_lat, param_lon], {icon: selected_icon})
                    .addTo(mymap)
                    .bindPopup("<p>" + popup_content + "</p>")
                    .openPopup()
                    // マーカークリックイベント
                    .on( 'click', function(e) {
                        if(Object.keys(manual_marker).length !== 0){
                            mymap.removeLayer(manual_marker);
                        }
                        selected_marker
                            .setIcon(selected_icon)
                            .bindPopup("<p>" + popup_content + "</p>");
                        // document.getElementById('latitude').value = e.latlng.lat;
                        // document.getElementById('longitude').value = e.latlng.lng;
                        document.getElementById('latitude').value = param_lat;
                        document.getElementById('longitude').value = param_lon;
                    });

        document.getElementById('latitude').value = param_lat;
        document.getElementById('longitude').value = param_lon;
    }

    /*
     * 手動（クリック）マーカーセット(マップ上のクリックした位置にマーカーをセットする)
     */
    function set_manual_marker(e, param_icon, popup_content){       
        if(Object.keys(manual_marker).length !== 0){
            mymap.removeLayer(manual_marker);
        }
        if (Object.keys(selected_marker).length !== 0) {
            selected_marker.setIcon(param_icon);
        }
        
        // クリック地点の位置経緯度を取得
        var click_point_lat = e.latlng.lat;
        var click_point_lon = e.latlng.lng;
        
        manual_marker = L.marker([click_point_lat, click_point_lon], {icon: selected_icon})
                            .addTo(mymap)
                            .bindPopup("<p>" + popup_content + "</p>")
                            .openPopup();

        // Zoom レベルが初期値より少ない場合は 15 に設定し、取得した緯度・経度にマップの中心を移動する
        if (mymap.getZoom() <= 12) {
            mymap.setView([click_point_lat, click_point_lon],15);   
        }

        document.getElementById('latitude').value = click_point_lat;
        document.getElementById('longitude').value = click_point_lon;
    }

    /*
     * 文字列変換
     *   ※ OpenStreetMapAPI では半角カナと全角英数字では、検索できない模様
     */
    // 英数字：全角→半角（@param スポット名フォーム入力値）
     function full2half_alphanumeric(input_str) {
        var converted_str = input_str.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
                                return String.fromCharCode(s.charCodeAt(0) - 65248);
                            });
        return converted_str;
    }
    // カナ：半角→全角（@param スポット名フォーム入力値の英数字を convert した値）
    function half2full_kana(converted_str) {
        var half_width_kana = 
            new Array('ｧ','ｨ','ｩ','ｪ','ｫ','ｬ','ｭ','ｮ','ｯ','ｰ','ｳﾞ',
                      'ｶﾞ','ｷﾞ','ｸﾞ','ｹﾞ','ｺﾞ',
                      'ｻﾞ','ｼﾞ','ｽﾞ','ｾﾞ','ｿﾞ',
                      'ﾀﾞ','ﾁﾞ','ﾂﾞ','ﾃﾞ','ﾄﾞ',
                      'ﾊﾞ','ﾋﾞ','ﾌﾞ','ﾍﾞ','ﾎﾞ',
                      'ﾊﾟ','ﾋﾟ','ﾌﾟ','ﾍﾟ','ﾎﾟ',
                      'ｱ','ｲ','ｳ','ｴ','ｵ',
                      'ｶ','ｷ','ｸ','ｹ','ｺ',
                      'ｻ','ｼ','ｽ','ｾ','ｿ',
                      'ﾀ','ﾁ','ﾂ','ﾃ','ﾄ',
                      'ﾅ','ﾆ','ﾇ','ﾈ','ﾉ',
                      'ﾊ','ﾋ','ﾌ','ﾍ','ﾎ',
                      'ﾏ','ﾐ','ﾑ','ﾒ','ﾓ',
                      'ﾔ','ﾕ','ﾖ',
                      'ﾗ','ﾘ','ﾙ','ﾚ','ﾛ',
                      'ﾜ','ｦ','ﾝ');
        var full_width_kana  = 
            new Array('ァ','ィ','ゥ','ェ','ォ','ャ','ュ','ョ','ッ','ー','ヴ',
                      'ガ','ギ','グ','ゲ','ゴ',
                      'ザ','ジ','ズ','ゼ','ゾ',
                      'ダ','ヂ','ヅ','デ','ド',
                      'バ','ビ','ブ','ベ','ボ',
                      'パ','ピ','プ','ペ','ポ',
                      'ア','イ','ウ','エ','オ',
                      'カ','キ','ク','ケ','コ',
                      'サ','シ','ス','セ','ソ',
                      'タ','チ','ツ','テ','ト',
                      'ナ','ニ','ヌ','ネ','ノ',
                      'ハ','ヒ','フ','ヘ','ホ',
                      'マ','ミ','ム','メ','モ',
                      'ヤ','ユ','ヨ',
                      'ラ','リ','ル','レ','ロ',
                      'ワ','ヲ','ン');
    
        for(var i = 0; i < half_width_kana.length; i++) {
            converted_str = converted_str.replace(new RegExp(half_width_kana[i], 'g'), full_width_kana[i]);
        }
        return converted_str; 
    }