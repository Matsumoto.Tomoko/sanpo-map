# SANPO MAP アプリ

## アプリ概要
散歩テーマを設定し、テーマに関連するスポットを登録することで、自分だけの散歩マップがつくれる。

![introduction_image](./docs/images/introduction.gif)

## 開発環境
[Laravel 5.5 Laravel Homestead](https://readouble.com/laravel/5.5/ja/homestead.html)

## 動作確認環境
Google Chrome 83.0.4103.61

## 使用ライブラリ・ツール等
- [Leaflet](https://leafletjs.com/)
	- Web地図のためのJavaScriptライブラリ。
- [OpenStreatMap](https://www.openstreetmap.org/)
	- OpenStreetMap財団 ( OSMF ) が Open Data Commons Open Database License ( ODbL ) の下にライセンスするオープンデータ。
- [Nominatim](https://wiki.openstreetmap.org/wiki/JA:Nominatim)
	- OpenStreetMap データを名前や住所で探したり、 OpenStreetMap ポイントの住所を合成して生成する（逆ジオコーディング）ツール。

## 機能一覧
ユーザーごとにアカウントを持ち、ログインしたユーザーはアカウント内で、散歩テーマ・スポットに関して以下のことができる。

### 作成機能
- 散歩テーマを登録する（例：春のお花見マップ、開運！神社仏閣めぐり）。
- 登録した散歩テーマごとにスポット情報（スポット名、緯度経度、 コメント）を登録できる。
	- 緯度経度の登録方法
		- スポット名検索により緯度経度情報を取得し、マップ上にマーカーを設置する。
		- スポット名検索の検索結果が複数あった場合には、マップ上に設置されたマーカーの中から該当の地点のマーカーを選択する（イメージ１）。
		- スポット名による検索が 0 件、または想定した地点が検索されなかった場合はユーザがマップ上をクリックした箇所にマーカーを設置する。緯度経度は設置されたマーカーから取得する（イメージ２）。

### 閲覧機能
- 散歩テーマを選択すると、そのテーマに紐づくスポット一覧がテーブルで表示される。また、スポットの緯度経度情報によりマップ上にマーカーが設置される。
- スポット一覧からスポットを選択すると、詳細情報を表示できる。

### 編集機能
- 登録済みの散歩テーマ名を編集できる。
- 登録済みのスポットの情報を編集できる。

### 削除機能
- 登録済みのスポットの情報を削除できる。
- 登録済みの散歩テーマを削除できる。その場合、その散歩テーマに紐づくスポットはすべて削除される。

## 詳細イメージ
### イメージ１
![register_image](/docs/images/register_spots.gif)
### イメージ２
![register_image](/docs/images/register_spot_manual.gif)