<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/themes/new', 'ThemesController@create')->name('themes.create');
    Route::post('/themes', 'ThemesController@store')->name('themes.store');

    Route::group(['middleware' => 'can:view,theme'], function() {
        Route::get('/themes/{theme}/spots', 'SpotsController@index')->name('spots.index');

        Route::get('/themes/{theme}/edit', 'ThemesController@edit')->name('themes.edit');
        Route::post('/themes/{theme}/update', 'ThemesController@update')->name('themes.update');
        Route::delete('/themes/{theme}', 'ThemesController@destroy')->name('themes.delete');

        Route::get('/themes/{theme}/spots/new', 'SpotsController@create')->name('spots.create');
        Route::get('/themes/{theme}/spots/new_manually', 'SpotsController@create_manually')->name('spots.create_manually');
        Route::post('/themes/{theme}/spots', 'SpotsController@store')->name('spots.store');
        Route::get('/themes/{theme}/spots/{spot}', 'SpotsController@show')->name('spots.show');
        Route::get('/themes/{theme}/spots/{spot}/edit', 'SpotsController@edit')->name('spots.edit');
        Route::post('/themes/{theme}/spots/{spot}/update', 'SpotsController@update')->name('spots.update');
        Route::delete('/themes/{theme}/spots/{spot}', 'SpotsController@destroy')->name('spots.delete');
    });
});