@extends('../layout')

@section('sub_title')
    テーマ名変更
@endsection

@section('link_to_home')
    <a href="{{ route('spots.index', ['id' => $theme->id]) }}" class="btn btn-outline-dark bg-light">Home</a>
@endsection

@section('content')
    <form class="form-signin" action="{{ route('themes.update', ['id' => $theme->id]) }}" method="post">
        {{ csrf_field() }}
        <fieldset>
        <div class="form-group">
            <label class="control-label" for="theme">テーマ名</label>
            <input type="text" class="form-control" name="theme" id="theme" value="{{ old('theme', $theme->theme) }}">
        </div>
        <div class="form-group">
            <input type="hidden" name="user_id" id="user_id" value="{{ $theme->user_id }}">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-outline-dark">登録</button>
        </div>
        </fieldset>
    </form>
@endsection
