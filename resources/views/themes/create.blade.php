@extends('../layout')

@section('sub_title')
    新規テーマ作成
@endsection

@section('link_to_home')
    <a href="{{ route('home') }}" class="btn btn-outline-dark bg-light">Home</a>
@endsection

@section('content')
    <form class="form-signin" action="{{ route('themes.store') }}" method="post">
        {{ csrf_field() }}
        <fieldset>
        <div class="form-group">
            <label class="control-label" for="theme">テーマ名</label>
            <input type="text" class="form-control" name="theme" id="theme" value="{{ old('theme') }}" placeholder="開運！神社仏閣めぐり">
        </div>
        <div class="form-group">
            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::id() }}">
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-outline-dark">登録</button>
        </div>
        </fieldset>
    </form>
    <div class="form-signin">
        <p>例えば...</p>
        <ul>
            <li>アートにどっぷり浸かる一日</li>
            <li>開運！神社仏閣めぐり</li>
            <li>いぬとおさんぽ</li>
        </ul>
    </div>
@endsection
