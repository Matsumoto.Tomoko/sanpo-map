@extends('../layout')

@section('sub_title')
    Error
@endsection

@section('content')
    <div class="w-75 mx-auto">
        <h3>お探しのページは見つかりませんでした。</h3>
    </div>
    <div class="text-center">
        <a href="{{ route('home') }}" class="btn btn-outline-dark">
            ホームへ戻る
        </a>
    </div>
@endsection