@extends('layout')

@section('additional_css')
    main{
        width: 60%;
        margin: 10vh auto;
    }
    @media screen and (max-width: 640px) {/* 640px以下*/
        main{
            width: 95%;
        }
    }
@endsection

@section('content')    
    <div class="list-group">
        <h2>ようこそ SANPO MAP へ！</h2>
        <p>SANPO MAP では、自分だけのお散歩用 MAP を作成することができます。</p>
        <div>
            <p>まずは散歩のテーマを登録しましょう！</p>
            <div>
                <a href="{{ route('themes.create') }}" class="btn btn-outline-dark">
                    テーマ登録ページへ
                </a>
            </div>
        </div>
    </div>
@endsection
