@extends('../layout_auth')

@section('sub_title')
    ユーザー登録
@endsection

@section('content')
    <form class="form-signin" name="refistform" action="{{ route('register') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name" class="control-label">ユーザー名</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
                placeholder="Sanpo_Man">
        </div>
        <div class="form-group">
            <label for="email" class="control-label">メールアドレス</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}"
                placeholder="sanpo_love@example.com">
        </div>
        <div class="form-group">
            <label for="password" class="control-label">パスワード</label>
            <input type="password" class="form-control" id="password" name="password"
                placeholder="Password(６文字以上)">
        </div>
        <div class="form-group">
            <label for="password_confirmation" class="control-label">パスワード（確認）</label>
            <input type="password" class="form-control" id="password-confirm" name="password_confirmation"
                placeholder="Password">
        </div>       
        <button type="submit" name="action" class="btn btn-primary btn-block mt-3" value="send">登録</button>
    </form>
    <div class="text-center">
        <a href="{{ route('home') }}">Login ページへもどる</a>
    </div>
@endsection