@extends('../layout_auth')

@section('content')
    <form class="form-signin" name="loginform" action="{{ route('login') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">メールアドレス</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}"
                placeholder="mysanpomap@example.com">
        </div>
        <div class="form-group">
            <label for="password">パスワード</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary btn-block mt-3" value="send">ログイン</button>
    </form>
    <div class="text-center">
        <a href="{{ route('password.request') }}">パスワードの変更はこちら</a>
    </div>
    <div class="text-center m-3">
        <a href="{{ route('register') }}">会員登録はこちら</a>
    </div>
@endsection