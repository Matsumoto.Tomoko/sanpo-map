@extends('../layout_auth')

@section('additional_css')
    .container{
        height: 100vh;
    }
    header{
        height: 15%;
    }
    main{
        margin: 10vh auto;
        width: 60%;
    }
@endsection

@section('sub_title')
パスワード再発行
@endsection

@section('content')
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <form action="{{ route('password.email') }}" method="POST" class="form-signin">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">メールアドレス</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ old('email')}}"/>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">再発行リンクを送る</button>
        </div>
    </form>
    <div class="text-center">
        <a href="{{ route('home') }}">Login ページへもどる</a>
    </div>
@endsection