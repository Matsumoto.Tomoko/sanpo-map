<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SANPO MAP</title>

        <!-- Leaflet -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <!-- Styles -->
        <style>
            html{
                font-family: 'Raleway', sans-serif;
            }
            body {
                background-image: url('{{ asset('image/background_2.jpg') }}');
                background-repeat: no-repeat;
                background-position: center center;
                background-attachment: fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            header{
                padding: 15px 0;
                color: #ffffff;
            }
            main{
                color: #444040;
                background: rgba(255, 255, 255, 0.85);
                border-radius: 5px;
                margin: auto;
                padding: 25px;
            }
            .display_area{
                width: 75%;
            }
            #mapid {
                height: 250px;
                width: 65%;
                margin: 1rem auto;
            }
            .form-control::placeholder{
                color: #d3d3d3;
            }
            h1{
                font-weight: 650;
            }
            .row{
                margin: 0;
            }
            .btn{
                font-size: 0.8rem;
            }
            .my_icon, .selected_icon, .candidate_icon{
                width: 20px !important;
                height: 20px !important;
                margin: -10px !important;
                border-radius: 10px;
                border: 3px solid #2f6066;
                background-color: #73ecfa;
                z-index: 20 !important;
            }
            .selected_icon{
                background-color: #ff4500;
                border: 3px solid #fdfdfd;
                box-shadow: 0 1px 4px rgba(0,0,0,0.8);
            }
            .candidate_icon{
                background-color: #00008b;
                border: 3px solid #fdfdfd;
                box-shadow: 0 1px 4px rgba(0,0,0,0.8);
            }
            @media screen and (max-width: 768px) {/* 768px以下*/
                html, body { font-size: 0.85rem;}
                main{ padding: 10px 0; }
                .display_area{width: 100%; }
                #mapid { height: 200px; width: 95%; }
                h1 { font-size: 2rem; }
                h2 { font-size: 1.5rem; }
                h3 { font-size: 1.2rem; }
                h4 { font-size: 1rem; }
                .lead{ font-size: 0.9rem; }
                .btn{ font-size: 0.7rem;}
            }
        </style>
    </head>
    <body>
        <div class="container">
            <header class="row">
                <div class="col-sm-12 col-md-6">
                    <h1>SANPO MAP</h1>
                    <h2>@yield('sub_title')</h2>
                </div>
                <nav class="col-sm-12 col-md-6 text-right">
                    @if(Auth::check())
                        <span class="lead">ようこそ, {{ Auth::user()->name }}さん</span>
                        @yield('link_to_home')
                        <a href="#" id="logout" class="btn btn-outline-dark bg-light">ログアウト</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </nav>
            </header>
            <main>
                <div class="container display_area">
                    {{-- エラーメッセージを受けっとた場合 --}}
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>     
                    @endif
                    @yield('content')
                </div>
            </main>
        </div>
    </body>
    <!-- Leaflet用スクリプト -->
    <script src="{{ asset('/js/leaflet/common_function.js') }}"></script>
    @yield('leaflet_script')
    <!-- ログアウト用スクリプト -->
    @if(Auth::check())
    <script>
        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('logout-form').submit();
        });
    </script>
    @endif
</html>