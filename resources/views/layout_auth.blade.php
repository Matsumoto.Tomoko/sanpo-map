<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SANPO MAP</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <!-- Styles -->
        <style>
            /* Pexelsのfreestocks.orgによる写真 */
            body {
                background-image: url('{{ asset('image/background_2.jpg') }}');
                background-repeat: no-repeat;
                background-position: center center;
                background-attachment: fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            .container{
                max-width: 1040px;
                height: 100vh;
                font-family: 'Raleway', sans-serif;
            }
            header{
                width: 100%;
                height: 15%;
                padding: 10px 0;
                color: #ffffff;
            }
            main{
                width: 70%;
                margin: 10vh auto;
                padding: 10px 20px;
                color: #707070;
                background: rgba(255, 255, 255, 0.85);
                border-radius: 5px;
            }
            .title{
                font-size: 5rem;
            }
            .form-control::placeholder{
                color: #d3d3d3;
            }
            .form-signin {
                width: 100%;
                max-width: 330px;
                padding: 30px 30px 15px 30px;
                margin: auto;
            }
            /* .row{
                margin: 0;
            }
            .btn{
                font-size: 0.8rem;
            } */
            @media screen and (max-width: 768px) {/* 768px以下*/
                html, body { font-size: 0.85rem; }
                main{ width: 90%; }
                .title { font-size: 3rem; }
            }
        </style>
    </head>
    <body>
        <div class="container">
            <header>
                <h1 class="title">SANPO MAP</h1>
                <h2>@yield('sub_title')</h2>
            </header>
            <main>
                <div class="display_area">
                    {{-- エラーメッセージを受けっとた場合 --}}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>        
                    @endif
                    @yield('content')
                </div>
            </main>
        </div>
    </body>
</html>