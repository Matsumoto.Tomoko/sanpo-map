<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SANPO MAP</title>

        <!-- Leaflet -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <!-- Styles -->
        <style>
            html{
                font-family: 'Raleway', sans-serif;
            }
            body {
                background-image: url('{{ asset('image/background_2.jpg') }}');
                background-repeat: no-repeat;
                background-position: center center;
                background-attachment: fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            .container_without_padding{
                padding: 0;
            }
            header{
                padding: 15px 0;
                color: #ffffff;
            }
            main{
                color: #444040;
                background: rgba(255, 255, 255, 0.85);
                border-radius: 5px;
                padding: 0 0 25px 0;
            }
            .theme_index{
                margin-top: 25px;
                padding-left: 10px;
            }
            .spot_index{
                margin-top: 25px;
                padding-right: 10px;
            }
            #mapid {
                height: 250px;
                width: 80%;
                margin: 1rem auto;
            }
            h1{
                font-weight: 650;
            }
            h3 img{
                height: 1.5rem;
                vertical-align: baseline;
                margin-right: 0.5rem;
            }
            .row{
                margin: 0;
            }
            .table{
                width: 95%;
                margin: 0 auto;
            }
            .table td{
                padding: 5px;
                vertical-align: middle;
            }
            .list-group-item {
                text-decoration: none;
                color: #6f6f6f;
            }
            .list-group-item:hover {
                background: #8e8e8e !important;
                color: #fdfdfd;
                text-decoration: none;
            }
            .list-group-item.active{
                background: #6f6f6f !important;
                border-color: #6f6f6f;
            }
            .btn{
                font-size: 0.8rem;
            }
            .my_icon{
                width: 20px !important;
                height: 20px !important;
                margin: -10px !important;
                border-radius: 10px;
                border: 3px solid #2f6066;
                background-color: #73ecfa;
                z-index: 20 !important;
            }
            .alert {
                margin-top: 1rem;
            }
            .invisible {
                display :none;
            }
            @media screen and (max-width: 768px) {/* 768px以下*/
                html, body { font-size: 0.85rem;}
                main{ padding: 10px 0; }
                .theme_index{ padding: 0; }
                .spot_index{ padding: 0; }
                #mapid { height: 200px; width: 95%; }
                h1 { font-size: 2rem; }
                h2 { font-size: 1.5rem; }
                h3 { font-size: 1.2rem; }
                h4 { font-size: 1rem; }
                .lead{ font-size: 0.9rem; }
                .btn{ font-size: 0.7rem;}
            }
        </style>
    </head>
    <body>
        <div class="container">
            <header class="row">
                <div class="col-sm-12 col-md-6">
                    <h1>SANPO MAP</h1>
                    <h2>My Page</h2>
                </div>
                <nav class="col-sm-12 col-md-6 text-right">
                    @if(Auth::check())
                    <span class="lead">ようこそ, {{ Auth::user()->name }}さん</span>
                    <a href="#" id="logout" class="btn btn-outline-dark bg-light">ログアウト</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    @endif
                </nav>
            </header>
            <main>
                <div class="container">
                    <div class="row">
                        {{-- エラーメッセージを受けっとた場合 --}}
                        @if ($errors->any())
                        <div class="col-7 alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        {{-- フラッシュ・メッセージ（登録・変更・削除完了）を受けっとた場合 --}}
                        @if (session('message'))
                        <div class="col-7 alert alert-success">
                            {{ session('message') }}
                        </div>
                        @endif
                    </div> 
                    <div class="row">
                        {{-- テーマ一覧 --}}
                        <nav class="theme_index col-sm-12 col-md-4">
                            <h3>
                                <img class="logo" src="{{ asset('image/icon_shoes.png') }}" alt="logo">SANPOテーマ
                            </h3>
                            <div class="text-center">
                                <a href="{{ route('themes.create', ['id' => $current_theme->id]) }}" class="btn btn-outline-dark m-2">
                                    テーマ追加
                                </a>
                            </div>
                            <div class="list-group mx-auto" style="width: 95%;">
                                @foreach($themes as $theme)
                                <a href="{{ route('spots.index', ['id' => $theme->id]) }}"
                                    class="list-group-item {{ $current_theme->id === $theme->id ? 'active' : '' }}">
                                        {{ $theme->theme }}
                                </a>
                                @endforeach
                            </div>
                        </nav>
                        {{-- スポット一覧 --}}
                        <div class="spot_index col-sm-12 col-md-8">
                            <h3>
                                <img class="logo" src="{{ asset('image/icon_shoes.png') }}" alt="logo">テーマ別スポット一覧
                            </h3>
                            <div class="container">
                                <div class="form-group">
                                    <div class="row">
                                        <h4 class="col-sm-12 col-lg-7">{{ $current_theme->theme }}</h4>
                                        <div class="col-sm-12 col-lg-5 form-inline justify-content-end">
                                            <a href="{{ route('themes.edit', ['id' => $current_theme->id]) }}" class="btn btn-outline-dark">テーマ名変更</a>
                                            <form action="{{ route('themes.delete', ['id' => $current_theme->id ]) }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="delete">
                                                <input type="submit" class="btn btn-outline-dark" value="テーマ削除">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- $spots が0件の場合表示 --}}
                            <div id="no_spot" style="display: none;" class="w-75 mx-auto mt-5">
                                <div>テーマの次は、テーマに沿ったスポットを登録していきます。<br>自分だけの散歩マップを作りましょう！</div>
                                <div class="text-center">
                                    <a href="{{ route('spots.create', ['id' => $current_theme->id]) }}" class="btn btn-outline-dark m-3">スポット追加</a>
                                </div>
                            </div>
                            {{-- $spots が1件以上の場合表示 --}}
                            <div id="has_spot" style="display: block;">
                                <div id="mapid"></div>
                                <div class="text-center">
                                    <a href="{{ route('spots.create', ['id' => $current_theme->id]) }}" class="btn btn-outline-dark m-2">スポット追加</a>
                                </div>
                                <table class="table">
                                    <tbody>
                                        @foreach ($spots as $spot)
                                        <tr>
                                            <td class="text-left">{{ $spot->name }}</td>
                                            <td class="text-right">
                                                <a href="{{ route('spots.show', ['id' => $spot->theme_id, 'spots_id' => $spot->id,]) }}" class="btn btn-outline-dark">詳細</a>
                                            </td>
                                        </tr> 
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </body>
    <!-- Leaflet用スクリプト -->
    <script>
        // php の変数 $spots → json へ
        window.spots = {}
        window.spots = @json($spots);
    </script>
    <script src="{{ asset('/js/leaflet/common_function.js') }}"></script>
    <script src="{{ asset('/js/leaflet/index.js') }}"></script>
    <!-- ログアウト用スクリプト -->
    @if(Auth::check())
    <script>
        document.getElementById('logout').addEventListener('click', function(event) {
            event.preventDefault();
            document.getElementById('logout-form').submit();
        });
    </script>
    @endif
</html>