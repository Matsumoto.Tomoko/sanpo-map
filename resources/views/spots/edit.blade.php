@extends('../layout_show_and_edit')

@section('sub_title')
    スポット情報変更
@endsection

@section('link_to_home')
    <a href="{{ route('spots.index', ['id' => $spot->theme_id]) }}" class="btn btn-outline-dark bg-light">Home</a>
@endsection

@section('content')
    <div>
        <div id="mapid"></div>
        <form class="form-horizontal" action={{ route('spots.update', ['id' => $spot->theme_id, 'spots_id' => $spot->id,]) }} method="post">
            {{ csrf_field() }}
            <fieldset>
            <div class="form-group">
                <label class="control-label" for="name">スポット名</label>
                <input type="text" class="form-control col-lg-5" name="name" id="name" value="{{ old('name', $spot->name) }}">
            </div>
            <div class="form-group">
                <label class="control-label" for="comment">コメント</label>
                <textarea class="form-control col-lg-10" rows="3" name="comment" id="comment">{{ old('comment', $spot->comment) }}</textarea>
            </div>
            <div class="form-group">
                <input type="hidden" name="latitude" id="latitude" value="">
                <input type="hidden" name="longitude" id="longitude" value="">
                <input type="hidden" readonly="readonly" name="theme_id" id="theme_id" value="<?php echo $spot->theme_id; ?>">
                <input type="hidden" readonly="readonly" name="id" id="id" value="<?php echo $spot->id; ?>">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-outline-dark mt-3">更新</button>
            </div>
            </fieldset>
        </form>
        <div class="text-center">
            <a href="{{ route('spots.show', ['id' => $spot->theme_id, 'spots_id' => $spot->id,]) }}" class="btn btn-outline-dark">
                前のページへもどる
            </a>
        </div>
    </div>
@endsection

@section('leaflet_script')
    <script src="{{ asset('/js/leaflet/edit.js') }}"></script>
@endsection
