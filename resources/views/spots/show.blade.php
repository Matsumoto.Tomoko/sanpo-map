@extends('../layout_show_and_edit')

@section('sub_title')
    スポット詳細情報
@endsection

@section('link_to_home')
    <a href="{{ route('spots.index', ['id' => $spot->theme_id]) }}" class="btn btn-outline-dark bg-light">Home</a>
@endsection

@section('content')
    <div>
        <dl class="row">
            <dt class="col-sm-3">スポット名</dt>
            <dd class="col-sm-9">{{ $spot->name }}</dd>
        </dl>
        <dl class="row">
            <dt class="col-sm-3">コメント</dt>
            <dd class="col-sm-9">{{ $spot->comment }}</dd>
        </dl>
    </div>
    <div id="mapid"></div>
    <div class="text-center">
        <a href="{{ route('spots.edit', ['id' => $spot->theme_id, 'spots_id' => $spot->id,]) }}" class="btn btn-outline-dark" role="button">
            変更
        </a>
        <form action="{{ route('spots.delete', ['id' => $spot->theme_id, 'spots_id' => $spot->id,]) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="delete">
            <input type="submit" class="btn btn-outline-dark mt-3" value="削除">
        </form>
    </div>
@endsection

@section('leaflet_script')
    <script src="{{ asset('/js/leaflet/show.js') }}"></script>
@endsection