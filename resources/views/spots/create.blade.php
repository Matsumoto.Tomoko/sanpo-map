@extends('layout')

@section('sub_title')
    新規スポット作成
@endsection

@section('link_to_home')
    <a href="{{ route('spots.index', ['id' => $theme_id]) }}" class="btn btn-outline-dark bg-light">Home</a>
@endsection

@section('content')
    <form class="form-group" action="{{ route('spots.store', ['id' => $theme_id]) }}" method="post">
        {{ csrf_field() }}
        <fieldset>
            {{-- name --}}
            <p class="lead mt-3">Step1 : 登録したいスポット名を入力し、検索ボタンを押してください</p>
            <div class="form-group ml-4">
                <label class="control-label mr-1" for="name">スポット名（必須）</label>
                <input type="text" class="mr-1" name="name" id="name" value="{{ old('name') }}" placeholder="SANPO公園">
                <button type="button" class="btn btn-outline-dark mr-1" id="serach_btn">検索</button>
                <button type="button" class="btn btn-outline-dark mr-1" id="reset_btn">リセット</button>
            </div>
            {{-- latitude / longitude --}}
            <p class="lead mt-3">Step2 : マップ上の該当の地点にマーカーを設置してください（マーカーの設置は必須です）</p>
            <div class="form-group">
                <ul id="search_result" class="alert-info">
                </ul>
                <div id="mapid"></div>
                <input type="hidden" name="latitude" id="latitude" value="">
                <input type="hidden" name="longitude" id="longitude" value="">
            </div>
            {{-- comment --}}
            <p class="lead mt-3">Step3 : コメントを入力し、登録ボタンを押してください</p>
            <div class="form-group ml-4">
                <label class="control-label" for="comment">コメント（必須）</label>
                <textarea class="form-control col-lg-10" name="comment" id="comment" rows="3" placeholder="緑豊かで犬の散歩にぴったり！">{{ old('comment') }}</textarea>
            </div>
            {{-- theme_id --}}
            <div class="form-group ml-4">
                <input type="hidden" readonly="readonly" name="theme_id" id="theme_id" value="<?php echo $theme_id; ?>">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-outline-dark m-3">登録</button>
            </div>
        </fieldset>
    </form>
@endsection

@section('leaflet_script')
    <script src="{{ asset('/js/leaflet/create.js') }}"></script>
@endsection
