<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=1; $i <= 3 ; $i++) { 
            $users =  [
                'name' => 'test' . $i,
                'email' => 'test' .$i .'@test.com',
                'password' => bcrypt('testtest'. $i),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            DB::table('users')->insert($users);
        }
    }
}
