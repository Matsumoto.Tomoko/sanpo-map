<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ja_JP');
        $now = \Carbon\Carbon::now();
        $lan = 35.690987;
        $lon = 139.701461;
        $add_1 = 0.01;

        for($i = 0; $i < 2; $i++){
            $spots = [
                'theme_id' => 1,
                'name' => $faker->country . '美術館',
                'comment' => '芸術は爆発だ！',
                'created_at' => $now,
                'updated_at' => $now,
                'latitude' => $lan + $add_1,
                'longitude' => $lon+ $add_1,
            ];
            DB::table('spots')->insert($spots);

            $add_1+=0.01;
        }

        for($i = 0; $i < 2; $i++){
            $spots = [
                'theme_id' => 2,
                'name' => $faker->country . '公園',
                'comment' => 'いやされる',
                'created_at' => $now,
                'updated_at' => $now,
                'latitude' => $lan + $add_1,
                'longitude' => $lon+ $add_1,
            ];
            DB::table('spots')->insert($spots);

            $add_1+=0.02;
        }

        for($i = 0; $i < 2; $i++){
            $spots = [
                'theme_id' => 3,
                'name' => $faker->country . '神社',
                'comment' => '開運！',
                'created_at' => $now,
                'updated_at' => $now,
                'latitude' => $lan + $add_1,
                'longitude' => $lon+ $add_1,
            ];
            DB::table('spots')->insert($spots);

            $add_1+=0.03;
        }
    }
}
