<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $themes = ['アートにふれる一日', '春らんまん！お花見ツアー', '開運！神社仏閣めぐり'];
        
        foreach ($themes as $theme) {
            DB::table('themes')->insert([
                'theme' => $theme,
                'user_id' => mt_rand(1, 3),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}