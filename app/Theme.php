<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function spots()
    {
        return $this->hasMany('App\Spot');
    }
}
