<?php

namespace App\Policies;

use App\User;
use App\Theme;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThemePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    } 
    /**
    * テーマの閲覧権限
    * @param User $user
    * @param Theme $theme
    * @return bool
    */
   public function view(User $user, Theme $theme)
   {
       return $user->id === $theme->user_id;
   }
}
