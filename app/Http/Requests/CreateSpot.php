<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateSpot extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:50',
                // spotsテーブルでユニーク制約
                Rule::unique('spots')->where(function($query) {
                    // theme_id の値と同じ値を持つレコードでのみ検証する
                    $query->where('theme_id', $this->input('theme_id'));
                }),
            ],
            'comment' => ['required', 'max:200'],
            'latitude' => [
                // spotsテーブルでユニーク制約
                Rule::unique('spots')->where(function($query) {
                    // longitude の値と同じ値を持つレコードでのみ検証する
                    $query->where('longitude', $this->input('longitude'))
                    // theme_id の値と同じ値を持つレコードでのみ検証する
                          ->where('theme_id', $this->input('theme_id'));
                }),
            ],
            'latlong' => ['required',],
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'スポット名',
            'comment' => 'コメント',
        ];
    }

    public function messages()
    {
        return [
            'latitude.unique' => '同一テーマ内で既に同じ位置にマーカーが設置されています',
            'latlong.required' => 'マップにマーカーを設置してください',
        ];
    }
    // 前処理
    protected function validationData()
    {
        $data = parent::validationData();

        // 緯度経度を DB と同じ小数点以下6桁に四捨五入する
        if ($data['latitude']){
            $data['latitude'] = round($data['latitude'],6);
        }
        $this->replace($data);
        if ($data['longitude']){
            $data['longitude'] = round($data['longitude'],6);
        }
        $this->replace($data);

        $data['latlong'] = null;

        // 緯度経度が揃っている場合に latlong に値を代入する
        if ($data['latitude'] && $data['longitude']) {
            $data['latlong'] = $data['latitude'] . '-' . $data['longitude'];
        }

        return $data;
    }
}
