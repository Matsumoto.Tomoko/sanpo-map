<?php

namespace App\Http\Requests;

use App\Spot;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EditSpot extends CreateSpot
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:50',
                // spotsテーブルでユニーク制約（編集中の id は除く）
                Rule::unique('spots')->ignore($this->input('id'))->where(function($query) {
                    // theme_id の値と同じ値を持つレコードでのみ検証する
                    $query->where('theme_id', $this->input('theme_id'));
                }),
            ],
            'comment' => ['required', 'max:200'],
            'latitude' => [
                // spotsテーブルでユニーク制約（編集中の id は除く）
                Rule::unique('spots')->ignore($this->input('id'))->where(function($query) {
                    // longitude の値と同じ値を持つレコードでのみ検証する
                    $query->where('longitude', $this->input('longitude'))
                    // theme_id の値と同じ値を持つレコードでのみ検証する
                          ->where('theme_id', $this->input('theme_id'));
                }),
            ],
            'latlong' => ['required',],
        ];
    }

    public function attributes()
    {
        $attributes = parent::attributes();
        return $attributes;
    }

    public function messages()
    {
        $messages = parent::messages();
        return $messages;
    }
}
