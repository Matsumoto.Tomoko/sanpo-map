<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateTheme extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'theme' => [
                'required',
                'max:100',
                // spotsテーブルでユニーク制約
                Rule::unique('themes')->where(function($query) {
                    // theme_id の値と同じ値を持つレコードでのみ検証する
                    $query->where('user_id', $this->input('user_id'));
                }),
            ],
            // 'theme' => ['required', 'unique:themes', 'max:100'],
        ];
    }

    public function attributes()
    {
        return [
            'theme' => 'テーマ名',
        ];
    }
}
