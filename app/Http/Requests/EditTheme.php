<?php

namespace App\Http\Requests;

use App\Theme;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EditTheme extends CreateTheme
{
    public function rules()
    {
        $rule = parent::rules();
        return $rule;
    }

    public function attributes()
    {
        $attributes = parent::attributes();
        return $attributes;
    }
}
