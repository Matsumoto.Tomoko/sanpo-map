<?php

namespace App\Http\Controllers;

use App\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateTheme;
use App\Http\Requests\EditTheme;

class ThemesController extends Controller
{
    /**
     * テーマ作成フォーム
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // themes.create ページをレスポンスする
        return view('themes/create');
    }
    /**
     * テーマ作成
     * @param CreateTheme $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateTheme $request)
    {
        // テーマモデルのインスタンスを作成
        $theme = new Theme();
        // 入力値を代入
        $theme->theme = $request->theme;
        $theme->user_id = $request->user_id;
        // ユーザーに紐付けてデータベースに保存
        $theme->save();

        return redirect()->route('spots.index', ['id' => $theme->id,])
                ->with('message', __('新しいテーマを登録しました'));
    }
    /**
     * テーマ編集フォーム
     * @param Theme $theme
     * @return \Illuminate\View\View
     */
    public function edit(Theme $theme)
    {
        return view('themes.edit', ['theme' => $theme,]);
    }
    /**
    * テーマ編集
    * @param Theme $theme
    * @param EditTheme $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(Theme $theme, EditTheme $request)
    {
        // 該当のテーマに入力値を入れて save する
        $theme->theme = $request->theme;
        $theme->user_id = $request->user_id;
        $theme->save();

        return redirect()->route('spots.index', ['id' => $theme->id,])
                ->with('message', __('テーマ名を更新しました'));
    }
    /**
    * テーマ削除
    * @param Theme $theme
    * @param Request $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(Theme $theme, Request $request)
    {
        $theme->delete();
        
        return redirect()->route('home')
                ->with('message', __('テーマを削除しました。'));
    }
    
}