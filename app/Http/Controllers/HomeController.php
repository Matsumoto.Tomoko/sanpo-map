<?php

namespace App\Http\Controllers;

use App\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        // ログインユーザーに紐づくテーマを一つ取得する
        $theme = Theme::where('user_id', Auth::id())->first();

        // まだ一つもテーマを作っていなければ home ページをレスポンスする
        if (is_null($theme)) {
            return view('home');
        }

        // テーマがあれば spots.index へリダイレクトする
        return redirect()->route('spots.index', [
            'id' => $theme->id,
        ]);
    }
}
