<?php

namespace App\Http\Controllers;

use App\Theme;
use App\Spot;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\CreateSpot;
use App\Http\Requests\EditSpot;
use SebastianBergmann\Environment\Console;

class SpotsController extends Controller
{
    /**
     * スポット一覧
     * @param Theme $theme
     * @return \Illuminate\View\View
     */
    public function index(Theme $theme)
    {
        // ユーザーのテーマを取得する
        $themes = Theme::where('user_id', Auth::id())->get();
        
        // 該当のテーマに紐づくスポットを取得する
        $spots = $theme->spots()->get();

        return view('spots/index', ['themes' => $themes, 'spots' => $spots, 'current_theme' => $theme,]);
        
    }
    /**
     * スポット作成フォーム
     * @param Theme $theme
     * @return \Illuminate\View\View
     */
    public function create(Theme $theme)
    {
        return view('spots/create', ['theme_id' => $theme->id,]);
    }
    /**
     * スポット作成
     * @param Theme $theme
     * @param CreateSpot $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Theme $theme, CreateSpot $request)
    {
        // スポットモデルのインスタンスを作成
        $spot = new Spot();
        // 入力値を代入
        $spot->theme_id = $request->theme_id;
        $spot->name = $request->name;
        $spot->comment = $request->comment;
        $spot->latitude = $request->latitude;
        $spot->longitude = $request->longitude;

        // 該当のテーマに紐づくスポットとしてデータベースに書き込む
        $theme->spots()->save($spot);

        return redirect()->route('spots.index', ['id' => $theme->id,])
                ->with('message', __('新しいスポットを登録しました。'));
    }
     /**
     * スポット詳細表示
     * @param Theme $theme
     * @param Spot $spot
     * @return \Illuminate\View\View
     */
    public function show(Theme $theme, Spot $spot)
    {
        $this->checkRelation($theme, $spot);
        
        return view('spots/show', ['spot' => $spot,]);
    }
    /**
     * スポット編集フォーム
     * @param Theme $theme
     * @param Spot $spot
     * @return \Illuminate\View\View
     */
    public function edit(Theme $theme, Spot $spot)
    {
        $this->checkRelation($theme, $spot);

        return view('spots/edit', ['spot' => $spot,]);
    }
     /**
     * スポット編集
     * @param Theme $theme
     * @param Spot $spot
     * @param EditSpot $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Theme $theme, Spot $spot, EditSpot $request)
    {
        $this->checkRelation($theme, $spot);

        // 該当のスポットに入力値を入れて save する
        $spot->theme_id = $theme->id;
        $spot->name = $request->name;
        $spot->comment = $request->comment;
        $spot->latitude = $request->latitude;
        $spot->longitude = $request->longitude;
        $spot->save();

        return redirect()->route('spots.index', ['id' => $spot->theme_id,])
                ->with('message', __('スポット情報を更新しました'));
    }
    /**
    * スポット削除
    * @param Theme $theme
    * @param Spot $spot
    * @param Request $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(Theme $theme, Spot $spot, Request $request)
    {
        $spot->delete();

        return redirect()->route('spots.index', ['id' => $spot->theme_id,])
                ->with('message', __('スポット情報を削除しました。'));
    }
    /**
    * リレーションチェック
    * @param Theme $theme
    * @param Spot $spot
    * @return abort(404)
    */
    private function checkRelation(Theme $theme, Spot $spot)
    {
        if ($theme->id !== $spot->theme_id) {
            abort(404);
        }
    }
}