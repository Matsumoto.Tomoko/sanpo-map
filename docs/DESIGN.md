# URL設計
http://homestead.mysanpomap
-----------------------------
|URL																	|メソッド						|処理																			|
|:---																	|:---							|:---																			|
|/themes/{テーマid}/spots			            						|GET 							|スポット一覧・テーマ一覧ページを表示する。										|
|/themes/new     				                						|GET 							|テーマ登録ページを表示する。													|
|/themes 			        	                						|POST 							|テーマ登録処理を実行する。														|
|/themes/{テーマid}/edit                 	    						|GET 							|テーマ編集ページを表示する														|
|/themes/{テーマid}/update	                    						|POST 							|テーマ編集処理を実行する。														|
|/themes/{テーマid}                      	    						|DELETE 						|テーマ削除処理を実行する（散歩テーマに紐づくお気に入りスポットも削除される）。 |
|/themes/{テーマid}/spots/new     										|GET 							|スポット登録ページを表示する。													|
|/themes/{テーマid}/spots			        							|POST 							|スポット登録処理を実行する。													|
|/themes/{テーマid}/spots/{スポットid}									|GET 							|スポット詳細を表示する。														|
|/themes/{テーマid}/spots/{スポットid}/edit								|GET 							|スポット編集ページを表示する													|
|/themes/{テーマid}/spots/{スポットid}/update&nbsp;&nbsp;&nbsp;&nbsp;	|POST 							|スポット編集処理を実行する。													|
|/themes/{テーマid}/spots/{スポットid}									|DELETE&nbsp;&nbsp;&nbsp;&nbsp; |スポット削除処理を実行する。													|

# テーブル定義

users
---------------
|カラム名							|データ型		|
|:---								|:--			|
|id									|AUTO_INCREMENT	|
|name								|VARCHAR(255)	|
|email								|VARCHAR(255)	|
|password							|VARCHAR()		|
|created_at 						|TIMESTAMP		|
|updated_at&nbsp;&nbsp;&nbsp;&nbsp; |TIMESTAMP		|

themes
---------------
|カラム名							|データ型			|
|:---								|:--				|
|id									|AUTO_INCREMENT		|
|theme								|VARCHAR(100)		|
|user_id							|INTEGER　unsigned	|
|created_at 						|TIMESTAMP			|
|updated_at&nbsp;&nbsp;&nbsp;&nbsp; |TIMESTAMP			|

spots
---------------
|カラム名							|データ型			|
|:---								|:--				|
|id									|AUTO_INCREMENT		|
|theme_id							|INTEGER　unsigned	|
|name								|VARCHAR(50)		|
|comment							|VARCHAR(200)		|
|latitude							|double(9, 6)		|
|longitude							|double(9, 6)		|
|created_at							|TIMESTAMP			|
|updated_at&nbsp;&nbsp;&nbsp;&nbsp; |TIMESTAMP			|